import { SportChallenges } from './sport-challenge/collections';
import { Challenge } from '../models/challenge';

Meteor.startup(function () {
    if (SportChallenges.find().count() === 0) {
        [
            new Challenge({
                type: 'pushups',
                userIds: ['1', '2'],
                count: 100,
                countType: 'everyone',
                userCounts: [
                    {
                        userId: '1',
                        count: 100
                    }, {
                        userId: '2',
                        count: 0
                    }
                ]
            }),
            new Challenge({
                type: 'situps',
                userIds: ['1', '2'],
                count: 500,
                countType: 'together',
                userCounts: [
                    {
                        userId: '1',
                        count: 0
                    }, {
                        userId: '2',
                        count: 0
                    }
                ]
            }),
            new Challenge({
                type: 'pushups',
                userIds: ['1', '2'],
                count: 100,
                countType: 'together',
                userCounts: [
                    {
                        userId: '1',
                        count: 100
                    }, {
                        userId: '2',
                        count: 100
                    }
                ]
            })
        ].forEach(function (challenge) {
            SportChallenges.insert(challenge);
        });
    }
});
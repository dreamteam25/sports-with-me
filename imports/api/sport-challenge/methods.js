import { Meteor } from "meteor/meteor";
import { check } from "meteor/check";

import { SportChallenges } from "./collections";

Meteor.methods({
  "challenge.insert"(data) {
    // check(text, String);

    // todo search if weekrecipe for user and week already exists
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }

    SportChallenges.insert({
      ...data,
      createdAt: new Date(),
      owner: this.userId,
      username: Meteor.users.findOne(this.userId).username,
    });
  },
  "challenge.updateUserCount"({ challengeid, userid, count }) {
    const challenge = SportChallenges.findOne({ _id: { $eq: challengeid } });

    console.log({count});

    const userCount = challenge.userCounts.find(user => user.userId === userid);

    if (!userCount) {
      throw new Meteor.Error("user-not-in-challenge");
    }

    userCount.count += count;

    SportChallenges.update(challenge._id, { $set: { userCounts: challenge.userCounts } });
  }
});

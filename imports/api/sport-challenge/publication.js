import { SportChallenges } from "./collections";

if (Meteor.isServer) {
  Meteor.publish("challenges", function challengesPublication() {
    return SportChallenges.find();
  });
}

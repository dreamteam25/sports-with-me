export class Challenge {
    type = ''; // zum beispiel liegestütze, kniebeuge
    userIds = ['']; // all users doing the challenge
    count = 100; //  every use has to do
    countType = ''; // "everyone", "together"
    userCounts = [ // count the users already did
        { 
            userId: '',
            count: 0
        }
    ];

    constructor(data) {
        Object.assign(this, data);
    }
}

export const challengeType = ['pushups', 'situps'];
export const countType = ['everyone', 'together'];
import { Meteor } from "meteor/meteor";
import Vue from "vue";
import VueRouter from "vue-router";
import router from "../imports/ui/router";

import VueMeteorTracker from "vue-meteor-tracker";
import vuetify from "../plugins/vuetify"; // path to vuetify export

Vue.use(VueMeteorTracker);
Vue.use(VueRouter);

import "../imports/startup/accounts.config.js";
import App from "../imports/ui/App.vue";

Meteor.startup(() => {
  new Vue({
    el: "#app",
    ...App,
    router,
    vuetify,
  });
});
